%-------------------------------------------------------------------------------
\newpage
\section{Electron Charge Mis-identification Studies}
\label{sec:chargeFlip}
%-------------------------------------------------------------------------------

Electron charge mis-identification is the cause of major background in
flagship ATLAS analyses such as same-sign $WW$
production~\cite{STDM-2014-05} , or $Z'$~\cite{EXOT-2016-05}
searches. The correct identification of the charge of a reconstructed
electrons depends strongly on the performance and geometry (in
particular the material distribution) of the tracking detector. In
this section studies of electron charge mis-identification with the
ITk geometry are compared to the corresponding performance obtained
with the Run 2 detector.

The charge of an electron is determined from the curvature of the
track matched to the electron cluster in the EM calorimeter. The
algorithm used for finding the best track candidate of a given cluster
is described in detail in Ref.~\cite{ATLAS-CONF-2016-024}.

Mis-measurement of the track curvature can cause, for high $p_T$
tracks, a charge mis-identification. However, in the case of
electrons the charge mis-identification is caused predominantly by
bremsstrahlung: the emitted photon can convert to an
electron--positron pair and generate one or two additional
reconstructed tracks matching the EM deposit in the calorimeter. Then,
depending on the kinematic of such a process (i.e. the fraction of
energy transferred to the photon, the scattering of the original
electron, etc..), one of this tracks from the $\gamma \to e^+ e^-$
conversion can be picked by the cluster-track matching algorithm as
``best'' match, practically randomizing the electron charge
determination.

%or traverse the
%first few layers of the detector without creating any track. In the
%first case, the cluster corresponding to the initial electron can be
%matched to the wrong-charge track, or most of the energy is
%ransferred from one track to the other because of the photon. In case
%of photon emission without subsequent pair production, the electron
%track has usually very few hits only in the first few layers, and thus
%a short lever arm on its curvature. Because the electron charge is
%derived from the track curvature, it could be incorrectly determined
%while the electron energy is likely appropriate as the emitted photon
%deposits all of its energy in the EM calorimeter as well.

Since the bremsstrahlung probability is directly linked to the
material (radiation length) traversed by the electron, the charge
mis-identification rate of low \pt electron is a sensitive
observable on the impact of the material budget of the new ITk
detector.

The charge mis-identification is studied using $Z\rightarrow ee$
simulated events. It is defined as the ratio of number of electrons
with incorrectly reconstructed charge to the number of all electrons,
$P(\text{flip}) = N(\text{flipped}) / N(\text{all})$.  Electrons with
the ``wrong'' charge are found by comparing the reconstructed charge
to the charge of the corresponding generated electron. Studies are
performed using both the ITk detector geometry and, with the same
analysis framework, the Run 2 detector geometry. In addition, for the
ITk geometry the studies are performed for different pile-up
conditions.

Variables commonly used for electron identification (i.e. $z_0$,
$d_0$, cluster-track matching criteria, etc...) are correlated with
the charge mis-identification probability. The optimization of the
electron identification criteria is done differently in the Run 2 and
HL-LHC environment, where different level of background is
present. Since in this contest we are mainly focused on the impact of
the material budget of the new ITk detector, to not bias the
comparison with the Run 2 configuration electron identification
requirements are not applied, except of course the minimal ones needed
to have a reconstructed electron object. The truth match with the
generated electrons mentioned above assures that the selected
electrons are indeed the ones from the $Z\rightarrow ee$ decay. Only
electrons with $E_{\text{T}} > \SI{25}{\GeV}$ are considered.

Resulting charge mis-identification probabilities are presented in
Figure~\ref{fig:chargeFlipRateEta} as a function of the electron
$|\eta|$.  The simulated charge probability is significantly smaller
with the ITk geometry than the Run 2 geometry by a factor of $3-5$,
depending on position of the electron in the detector. For what it has
been explained before, this lower probability can be mainly attributed
to the reduced material budget of the ITk detector.

The charge mis-identification probability is also found to be fairly
constant as a function of the electron \pt, in the range tested with
electrons from $Z\rightarrow ee$ decay.

As expected, no significant pile-up dependence is found as well since
the probability for a spurious track from other primary vertexes to be
picked as the ``best'' match by the cluster-track algorithm does not
significantly increase from an environment with a $<\mu>=60$ to a
$<\mu>=200$.


\begin{figure}[htpb]
\centering
\includegraphics[width=0.9\textwidth]{chargeFlipRateEta}
\caption{ Electron charge mis-identification probability as a function
  of the true $|\eta|$.  The charge mis-identification probability is
  compared across different geometries and pile-up values.  }
\label{fig:chargeFlipRateEta}
\end{figure}


% All figures and tables should appear before the summary and conclusion.
% The package placeins provides the macro \FloatBarrier to achieve this.
\FloatBarrier
